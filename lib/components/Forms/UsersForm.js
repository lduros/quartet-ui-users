"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddUser = exports._AddUser = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


var _GroupDialog = require("./Dialogs/GroupDialog");

var _PermissionDialog = require("./Dialogs/PermissionDialog");

var _users = require("../../reducers/users");

const React = qu4rtet.require("react");
const { Component } = React;
const { Card } = qu4rtet.require("@blueprintjs/core");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const PageForm = qu4rtet.require("./components/elements/PageForm").default;
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { reduxForm } = qu4rtet.require("redux-form");
const changeFieldValue = qu4rtet.require("redux-form").change;
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");

const UserForm = reduxForm({
  form: "UserForm"
})(PageForm);

class _AddUser extends Component {
  constructor(props) {
    super(props);

    this.toggleGroupDialog = evt => {
      this.setState({ isGroupOpen: !this.state.isGroupOpen });
    };

    this.togglePermissionDialog = evt => {
      this.setState({ isPermissionOpen: !this.state.isPermissionOpen });
    };

    this.processFields = (postValues, props) => {
      // Django wants the first and last name defined, even if
      // it's an empty string. Won't accept not having them
      // in the post.
      if (!("first_name" in postValues)) {
        postValues["first_name"] = "";
      }
      if (!("last_name" in postValues)) {
        postValues["last_name"] = "";
      }

      if (postValues["first_name"] === null) {
        // Django expects an empty string, not null,
        // unlike every other model in the world.
        postValues["first_name"] = "";
      }
      if (postValues["last_name"] === null) {
        postValues["last_name"] = "";
      }
      // replace m2m object of objects with array of ids only.
      Object.keys(postValues).forEach(field => {
        if (["user_permissions", "groups"].includes(field)) {
          if (postValues[field] instanceof Array) {
            // convert array of [item1, item2] into [id1, id2]
            postValues[field] = postValues[field].map(item => {
              if (item instanceof Object) {
                return item.id;
              }
              return item;
            });
          }
        }
      });
      if (!("groups" in postValues)) {
        postValues["groups"] = [];
      }
      if (!("user_permissions" in postValues)) {
        postValues["user_permissions"] = [];
      }
    };

    this.updateM2MValues = (formName, fieldName, objects) => {
      let user = this.state.user;
      if (user) {
        // we update the predefined user as to not replace
        // the picked items with the initial values each time.
        user[fieldName] = Object.keys(objects).map(key => {
          return objects[key];
        });
        this.setState({ user: user });
      }
      this.props.changeFieldValue(formName, fieldName, objects);
    };

    this.state = {
      formStructure: [],
      isGroupOpen: false,
      isPermissionOpen: false
    };
    this.state.user = null;
    if (this.props.location && this.props.location.state && this.props.location.state.defaultValues) {
      // to prepopulate with existing values.
      this.state.user = this.props.location.state.defaultValues;
    }
  }


  render() {
    const editMode = !!(this.props.location && this.props.location.state && this.props.location.state.edit);
    return React.createElement(
      RightPanel,
      {
        title: !editMode ? React.createElement(FormattedMessage, { id: "plugins.users.addUser" }) : React.createElement(FormattedMessage, { id: "plugins.users.editUser" }) },
      React.createElement(
        "div",
        { className: "large-cards-container" },
        React.createElement(
          Card,
          { className: "form-card" },
          React.createElement(
            "h5",
            null,
            !editMode ? React.createElement(FormattedMessage, { id: "plugins.users.addUser" }) : React.createElement(FormattedMessage, { id: "plugins.users.editUser" })
          ),
          React.createElement(UserForm, {
            edit: false,
            operationId: editMode ? "user_update" : "user_create",
            objectName: "user",
            djangoPath: "user/",
            submitPrecall: this.processFields,
            existingValues: this.state.user,
            redirectPath: `/users/${this.props.server.serverID}/users/`,
            parameters: this.state.user ? { id: this.state.user.id } : {},
            server: pluginRegistry.getServer(this.props.server.serverID),
            history: this.props.history,
            fieldElements: {
              groups: React.createElement(_GroupDialog.GroupDialog, _extends({}, this.props, {
                formName: "UserForm",
                changeFieldValue: this.updateM2MValues,
                isGroupOpen: this.state.isGroupOpen,
                toggleGroupDialog: this.toggleGroupDialog,
                existingValues: this.state.user,
                groups: this.props.groups || {}
              })),
              user_permissions: React.createElement(_PermissionDialog.PermissionDialog, _extends({}, this.props, {
                formName: "UserForm",
                changeFieldValue: this.updateM2MValues,
                isPermissionOpen: this.state.isPermissionOpen,
                togglePermissionDialog: this.togglePermissionDialog,
                existingValues: this.state.user,
                permissions: this.props.permissions || {}
              }))
            }
          })
        )
      )
    );
  }
}

exports._AddUser = _AddUser;
const AddUser = exports.AddUser = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.users.servers && state.users.servers[ownProps.match.params.serverID];
  };
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    groups: isServerSet() ? state.users.servers[ownProps.match.params.serverID].groups : [],
    count: isServerSet() ? state.users.servers[ownProps.match.params.serverID].count : 0,
    next: isServerSet() ? state.users.servers[ownProps.match.params.serverID].next : null,
    theme: state.layout.theme,
    permissions: isServerSet() ? state.users.servers[ownProps.match.params.serverID].permissions : []
  };
}, { loadGroups: _users.loadGroups, changeFieldValue, loadPermissions: _users.loadPermissions })(_AddUser);