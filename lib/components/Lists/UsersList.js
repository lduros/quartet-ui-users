"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UsersList = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var _users = require("../../reducers/users");

const React = qu4rtet.require("react");
const { Component } = React;
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { PaginatedList } = qu4rtet.require("./components/elements/PaginatedList");
const { DeleteObject } = qu4rtet.require("./components/elements/DeleteObject");
const { Tag, Intent } = qu4rtet.require("@blueprintjs/core");

const UsersTableHeader = props => React.createElement(
  "thead",
  { style: { textAlign: "center", verticalAlign: "middle" } },
  React.createElement(
    "tr",
    null,
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.users.username" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.users.first_name" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.users.last_name" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.users.email" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.users.userStatus" })
    )
  )
);

const UsersEntry = props => {
  const goTo = path => {
    props.history.push(path);
  };
  const goToPayload = goTo.bind(undefined, {
    pathname: `/users/${props.server.serverID}/add-user`,
    state: { defaultValues: props.entry, edit: true }
  });
  let deleteObj = DeleteObject ? React.createElement(DeleteObject, {
    entry: props.entry,
    operationId: "user_delete",
    server: props.server,
    title: React.createElement(FormattedMessage, { id: "plugins.users.deleteUserConfirm" }),
    body: React.createElement(FormattedMessage, { id: "plugins.users.deleteUserConfirmBody" }),
    postDeleteAction: props.loadEntries
  }) : null;
  return React.createElement(
    "tr",
    { key: props.entry.id },
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.username
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.first_name
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.last_name
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.email
    ),
    React.createElement(
      "td",
      { onClick: goToPayload, style: { textAlign: "center" } },
      props.entry.is_staff ? React.createElement(
        Tag,
        { intent: Intent.SUCCESS, style: { marginBottom: "10px" } },
        React.createElement(FormattedMessage, { id: "plugins.users.is_staff" })
      ) : null,
      React.createElement("br", null),
      props.entry.is_active ? React.createElement(
        Tag,
        { intent: Intent.SUCCESS, style: { marginBottom: "10px" } },
        React.createElement(FormattedMessage, { id: "plugins.users.is_active" })
      ) : null,
      React.createElement("br", null),
      props.entry.is_superuser ? React.createElement(
        Tag,
        { intent: Intent.SUCCESS, style: { marginBottom: "10px" } },
        React.createElement(FormattedMessage, { id: "plugins.users.is_superuser" })
      ) : null
    ),
    React.createElement(
      "td",
      null,
      deleteObj
    )
  );
};

class _UsersList extends Component {
  render() {
    const { server, users, loadUsers, count, next } = this.props;
    return React.createElement(
      RightPanel,
      { title: React.createElement(FormattedMessage, { id: "plugins.users.usersList" }) },
      React.createElement(
        "div",
        { className: "large-cards-container full-large" },
        React.createElement(PaginatedList, _extends({}, this.props, {
          listTitle: React.createElement(FormattedMessage, { id: "plugins.users.usersList" }),
          history: this.props.history,
          loadEntries: loadUsers,
          server: server,
          entries: users,
          entryClass: UsersEntry,
          tableHeaderClass: UsersTableHeader,
          count: count,
          next: next
        }))
      )
    );
  }
}

const UsersList = exports.UsersList = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.users.servers && state.users.servers[ownProps.match.params.serverID];
  };
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    users: isServerSet() ? state.users.servers[ownProps.match.params.serverID].users : [],
    count: isServerSet() ? state.users.servers[ownProps.match.params.serverID].count : 0,
    next: isServerSet() ? state.users.servers[ownProps.match.params.serverID].next : null
  };
}, { loadUsers: _users.loadUsers })(_UsersList);