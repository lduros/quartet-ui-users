"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _UsersList = require("./components/Lists/UsersList");

var _GroupsList = require("./components/Lists/GroupsList");

var _UsersForm = require("./components/Forms/UsersForm");

var _GroupsForm = require("./components/Forms/GroupsForm");

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const React = qu4rtet.require("react");
const { Route } = qu4rtet.require("react-router");

exports.default = (() => {
  return [React.createElement(Route, {
    key: "usersList",
    path: "/users/:serverID/users",
    component: _UsersList.UsersList
  }), React.createElement(Route, {
    key: "groupsList",
    path: "/users/:serverID/groups",
    component: _GroupsList.GroupsList
  }), React.createElement(Route, {
    key: "addUser",
    path: "/users/:serverID/add-user",
    component: _UsersForm.AddUser
  }), React.createElement(Route, {
    key: "addGroup",
    path: "/users/:serverID/add-group",
    component: _GroupsForm.AddGroup
  })];
})();