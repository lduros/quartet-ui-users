"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.disablePlugin = exports.enablePlugin = undefined;

var _UsersNav = require("./components/Nav/UsersNav");

var _styles = require("./styles");

var _styles2 = _interopRequireDefault(_styles);

var _messages = require("./messages");

var _messages2 = _interopRequireDefault(_messages);

var _routes = require("./routes");

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const actions = qu4rtet.require("./actions/plugins").default;
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const reducer = require("./reducers/users").default;
const { initialData } = require("./reducers/users");

const PLUGIN_NAME = "Users";

var fs = require("fs");
/* At runtime, read css as text */
require.extensions[".css"] = function (module, filename) {
  module.exports = fs.readFileSync(filename, "utf-8");
};

const enablePlugin = exports.enablePlugin = () => {
  pluginRegistry.registerReducer(PLUGIN_NAME, "users", reducer);
  pluginRegistry.registerRoutes(PLUGIN_NAME, _routes2.default);
  pluginRegistry.setMessages(_messages2.default);
  pluginRegistry.registerCss(PLUGIN_NAME, _styles2.default);
  pluginRegistry.registerComponent(PLUGIN_NAME, _UsersNav.UserManagementRoot, actions.addToTreeServers);
};

const disablePlugin = exports.disablePlugin = () => {
  pluginRegistry.unregisterRoutes(PLUGIN_NAME);
  pluginRegistry.unregisterComponent(PLUGIN_NAME, _UsersNav.UserManagementRoot, actions.removeFromTreeServers);
  pluginRegistry.unregisterCss(_styles2.default);
};